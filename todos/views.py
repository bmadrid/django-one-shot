from django.shortcuts import render
from .models import TodoList


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "list.html", context)
